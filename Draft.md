Basic Storage Contract (not m-of-n but whole)
=============================================

To implement m-of-n storage, run this contract n times on different chunks
	
Needs to support Read & Write
Also needs to function like a contract
- Minimum bandwidth
- Response time
- Maybe length of time contract is active
- Enforcement of correctness of reads/writes
- Payment agreements; either per-chunk or per-operation (for now let's say per-operation)
	- Per-operation is nicer because it doesn't require a contract call every X kb
	- Pay.R wei per successful read of chunk
	- Pay.W wei per successful write of chunk
	- Simplest would be pay-per-unit time
			
- Maximum storage space	

Ability to hold money in escrow for withholding payments

Before, I thought of a Post(data) operation which my EHS idea is modeled after
1. Post block publicly
2. Contract begins once correct hash is returned, maybe with a nonce
	
Instead, initial writing looks as follows:
1. Contract signing/agreement, which allocates empty space in node's memory
2. Write as ordinary
		
An instance of the above, then, is a tuple (Size, Speed, Pay (=PR, PW), MaxLatency, Expiration)
For now let's say that agreement is public
			
Ask(Size, Speed, PR, PW, MaxLatency, Expiration): create contract, set state = "Asking"; set client = msg.sender
Agree() : set state = "Agreed"; set server = msg.sender; send address of client to server, and vice versa
	Need to be able to prevent DDOS attacks by answering and then letting MaxLatency run out; maybe there's a little stake in the beginning
EnforceQuery(nonce, commit) / EnforceAnswer(commit) / EnforceOpen(hsh) / EnforceFinalize:
	commitment game to test _online_ that node actually has its block
	EnforceQuery should send a message to server that it is being audited
	run every few blocks or so; could be its own contract;
	One way this could work w/o client needing to be able to produce sha3(data || nonce) is that the challenge hash is actually sha3(sha3(data) || nonce)
	
	To test offline, heartbeat or similar can be used; if an offline enforcement fails, then online enforcement is used so the contract can be justified in taking action
	
		When enforcement fails, both parties end communication and something happens to deposit (returns to client?)
			credit returned to client
			
AddCredit(): add msg.value to contract's payment balance
RequestPay(): under certain conditions, pay server what is owed to them
	
To add: more auditing for MaxLatency
	
	
All actual communication happens offline, so this is the basic structure of a communication:
	
1. Client calls Ask
2. Server calls Agree
3. Offline reads/writes happen
4. Heartbeats happen offline
5. If a heartbeat fails, Enforce happens online
6. Pay happens periodically
7. If Expiration is passed, desposits are returned and contract ends; credit returns to client
	
To finish drafting: how payment happens? what happens if enforce fails? Are there any obvious loopholes in the above plan?
